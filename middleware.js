var middleware = {
    requireAuth: function (req, res, next) {
     console.log("private!");
     next();
    },
    logger: function (req, res, next) {
     var date = new Date().toString();
     console.log("Request at: " + date + " | Method: " +  req.method + " | For: " + req.originalUrl);
     next();
    }  
}

module.exports = middleware;